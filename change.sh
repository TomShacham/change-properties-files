#! /bin/bash

set -o errexit # exit as soon as any command fails

my_name=change-properties

my_purpose='change a properties file'

my_usage="Usage: $my_name [option]... PROPERTIES-FILE <to-dev|to-local> SERVICE...
  -q, --quiet      be quiet
      --help       print this help, then exit
"

function log_error() {
  echo "$@" >&2
}

function fail_with() {
  log_error "$@"
  exit 1
}

function get_opts() {
  # Process command-line options

  # Defaults
  opt_quiet=false
  opt_step=1

  while [ $# -gt 0 ]; do
    case "$1" in
      -q | --quiet)
        opt_quiet=true
        ;;
      --help)
        log_error '`'"$my_name': $my_purpose."
        log_error ''
        log_error "$my_usage"
        exit 0
        ;;
      --)
        shift
        break
        ;;
      -*)
        log_error "$my_name: unrecognized option "'`'"$1'"
        log_error 'Try `'"$my_name --help' for more information."
        exit 1
        ;;
      *)
        break
        ;;
    esac
    shift
  done

  file="$1"
  [ -n "$file" ] || fail_with "Missing filename argument"
  shift

  environment="$1"
  [ -n "$environment" ] || fail_with "Missing environment argument"
  shift

  # Save the remaining part of the command line
  services="$@"
}

function log_info() {
  $opt_quiet || echo "$@" >&2
}

function check_opts_and_args() {
  [ -z "$ARGS" ] || fail_with "Unexpected additional arguments: $ARGS"

  [ -n "$services" ] || fail_with "Need at least one service"
}

function mk_tmp_dir() {
  # Make a temporary directory for this script to use
  tmp="${TMPDIR=/tmp}/${my_name}$$"
  trap 'rm -rf "$tmp" >/dev/null 2>&1' 0 # remove $tmp at program termination
  trap 'exit 2' 1 2 3 13 15 # remove $tmp after receiving a signal
  mkdir "$tmp"
}

function change_properties() {
  service="$1"

  case "$environment" in
    *Dev)
      awk "c-->0;/^#\s*$service$/{c=4}" "$file" |
      sed -e '/[Dd][Ee][Vv]/ { n; s/^#//; }' |
      sed -e '/[Ll][Oo][Cc][Aa][Ll]/{n; s/^[^#]/#&/;}' > "$tmp"/tmp
      sed -i -e "/$service/{n;N;N;N;d;}" "$file"
      sed "/$service/r $tmp/tmp" "$file" > "$tmp"/newPropertiesFile
      cp -f "$tmp"/newPropertiesFile "$file"
      ;;
    *Local)
      awk "c-->0;/$service/{c=4}" "$file" |
      sed -e '/[Dd][Ee][Vv]/ { n; s/^[^#]/#&/; }' |
      sed -e '/[Ll][Oo][Cc][Aa][Ll]/{n; s/^#//;}' > "$tmp"/tmp
      sed -i -e "/$service/{n;N;N;N;d;}" "$file"
      sed "/$service/r $tmp/tmp" "$file" > "$tmp"/newPropertiesFile
      cp -f "$tmp"/newPropertiesFile "$file"      
      ;;
  esac
  
}

get_opts "$@"
check_opts_and_args
mk_tmp_dir

for service in $services; do
  change_properties $service
done
