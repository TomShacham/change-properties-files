Set up your properties file as follows:
---

    #<name of service>
    #<name of environment>
    endpoint
    #<name of environment>
    #endpoint

e.g.

    # UserService
    # local
    www.local.com/user/service
    # dev
    #www.dev.com/user/service

    # AnotherService

...and so on.

Now to change from local to dev environment run ./changeEnvironment.sh <name of properties file> toDev <list of services as args>

e.g.

./changeEnvironment app.properties toDev UserService AnotherService

Assumptions
----

- Information separated on each line as above.
- Name of service supplied as arg is case sensitive
- Name of environment is case sensitive
- Comment/uncomment with \#