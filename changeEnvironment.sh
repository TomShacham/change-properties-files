changeProperties () {
  for lastArg; do true; done; #massive hack that sets lastArg to last arg supplied

  if [[ $2 == *Dev* ]]
  then
    awk "c-->0;/$lastArg/{c=4}" $1 |
    sed -e '/[Dd][Ee][Vv]/ { n; s/^#//; }' |
    sed -e '/[Ll][Oo][Cc][Aa][Ll]/{n; s/^[^#]/#&/;}' > tmp
    sed -i -e "/$lastArg/{n;N;N;N;d;}" $1
    sed "/$lastArg/r tmp" $1 > newPropertiesFile
    cp -f newPropertiesFile $1
  elif [[ $2 == *Local* ]]
  then
    awk "c-->0;/$lastArg/{c=4}" $1 |
    sed -e '/[Dd][Ee][Vv]/ { n; s/^[^#]/#&/; }' |
    sed -e '/[Ll][Oo][Cc][Aa][Ll]/{n; s/^#//;}' > tmp
    sed -i -e "/$lastArg/{n;N;N;N;d;}" $1
    sed "/$lastArg/r tmp" $1 > newPropertiesFile
    cp -f newPropertiesFile $1
  fi
}


if [ $# -lt 3 ]
then
  echo "Too few arguments
Call with filename followed by env to change to and services to change"
else
  file=$1
  args=("$@")
  while [ ${#args[@]} -ge 3 ]
  do
    changeProperties ${args[@]}
    unset args[${#args[@]}-1] #pop last arg off array (shift pops first :[ )
  done
  rm newPropertiesFile
  rm tmp
  rm "$file-e"
fi